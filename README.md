# bi-template

proyecto base de business intelligence 

## Evalaución y Diseño
- Duración: 40 horas, 1 sprints (2 semanas)
- Metodología: Se porponen 1- 5 juntas de levantamiento de requerimientos

### Tareas

- [ ] Entendimiento de la estructura de datos
- [ ] Entendimiento de KPIs
- [ ] Definición de alcance en el despliegue de resultados

### Entregables  
- [ ] Propuesta de adaptación de infraestructura
- [ ] Propuesta inicial de Tableros o conjuntos de gráficas
- [ ] Propuesta de desarrollo de los tableros

## Adaptación de infraestructura

- Duración: 40-120 horas, 1-3 sprints (2-6 semanas)
- Metodología: Desarrollo con retroalimentación continua para contruir o adaptar los flujos de datos.

### Entregables  
- [ ] Repositorio en Gitlab con definición de flujos, ejemplos: cronjobs, microservicios, etc.
- [ ] Documentación
- [ ] Sesión de despliegue y traspaso 

## Definición de Tableros

- Duración: 40-80 horas, 1-2 sprint (2-4 semanas)
- Metodología: Diseño con retroalimentación continua de los gráficos que muestran los KPI

### Entregables  
- [ ] Repositorio en Gitlab con analisis y gráficos.
- [ ] Documentación
- [ ] Sesión de despliegue y traspaso

## Desarrollo de Tableros

- Duración: 40-80 horas, 1-2 sprint (2-4 semanas)
- Metodología: Desarrollo con retroalimentación continua de los gráficos que muestran los KPI

### Entregables  
- [ ] Repositorio en Gitlab con procesos end-to end: repositorio de datos - herramienta de despliegue.
- [ ] Documentación
- [ ] Sesión de despliegue y traspaso


